from flask import Flask, request, Response, jsonify
import resize
app = Flask(__name__)

class ApiException(Exception):
    status_code = 400
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message

@app.errorhandler(ApiException)
def handleApiException(ex):
	app.logger.error("API ERROR: %s" % ex.message)
	return ex.message, ex.status_code

def paramOrDefault(dict, key, transformer, defaultValue):
	if not key in dict: return defaultValue
	value = dict[key]
	try:
		return transformer(value)
	except ValueError as err:
		raise ApiException("Invalid %(key)s value %(value)s: %(err)s" % locals())

@app.route("/debug", methods=["GET"])
def debugHandler():
	import socket, os
	return jsonify(dict(
		pid=os.getpid(),
		hostname=socket.gethostname()
	))

@app.route("/resize", methods=["POST"])
def resizeHandler():
	if len(request.data) == 0: raise ApiException("Missing image data")
	width = paramOrDefault(request.args, "width", int, 100)
	height = paramOrDefault(request.args, "height", int, 100)
	try:
		bytes, mime = resize.resize(request.data, width, height)
		return Response(bytes, mimetype=mime)
	except Exception as err:
		raise ApiException("Failed to resize: %s" % str(err))

if __name__ == "__main__":
    app.run(host='0.0.0.0')
