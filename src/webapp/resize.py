from PIL import Image
import io

def resize(imgbytes, width, height):
	with Image.open(io.BytesIO(imgbytes)) as im:
		resized = im.resize((width, height))
		output = io.BytesIO()
		resized.save(output, format=im.format)
		return output.getvalue(), Image.MIME[im.format]
