import unittest

import  resize

class TestStringMethods(unittest.TestCase):

    def test_shouldHaveSameMimeTypeAsSource(self):
        bytes = open("sample.png", "rb").read()
        b, m = resize.resize(bytes, 100, 100)
        self.assertEqual("image/png", m)


if __name__ == '__main__':
    unittest.main()