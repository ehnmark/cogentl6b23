# Cogent assignment

## Requirement

The assignment was to build a web API for image resizing. It had to use Docker Compose, have at least some tests, and be easy to run. No frontend was required.

## Thoughts and Approach

There are two parts to this assignment: API design and scalability. Let's explore each.

Regarding API design, both request and response formats are open questions. We know that the request should contain an image. Although not required in this case, it may also contain resize criteria. JSON and multipart form requests are both valid options. However, I would argue that sending the source image as POST payload with optional resizing criteria as query parameters is more intuitive and "curl-friendly".

As image resizing is potentially a long-running operation, there are a few different ways one could model the response. For example, the request could return an identifier used by the consumer to poll or subscribe to progress updates. This approach lends itself well to a producer-consumer backend design, where the request handler forwards the request to workers via a distributed queue, and where workers post updates and final result back on another queue. However, this puts an additional burden on the API consumer.

Another approach is to combine web request handler and resize functionality into a single process but run multiple processes with a load-balancing reverse proxy in front of them. With this design, each worker process would synchronously process web requests, resize (which is a CPU-bound workload), and return the final result to the API consumer. As clients do not need to poll or subscribe to updates, load-balancing becomes a relatively simple task, with no concerns about session affinity.

In order to avoid needless complexity, both in terms of system design and API usage, I decided on the latter approach.


## Components

In order to route external web requests to one or more worker nodes, we need a reverse proxy. Next, the reverse proxy needs to talk to the actual web instances. We need a component that manages these processes, load balances requests, talks to them using Python's standard WSGI interface. Finally, we need the actual web instances. These run small, stateless, WSGI-compatible web servers as well as image resizing logic.

The specific technologies used were all chosen for the same reason: Actively-developed, well-understood, and proven: Nginx as reverse proxy, uWSGI as WSGI gateway and process manager, Flask as web framework, and Pillow for image resizing.

    Nginx -> uWSGI -> [Flask -> Pillow] (worker #1)
                   -> [Flask -> Pillow] (worker #2)
                   ...
                   -> [Flask -> Pillow] (worker #n)


## Web API

### POST /resize

_Body_ should be image data in a PIL-compatible format, e.g. JPEG or PNG, with an appropriate `Content-type` header. _Optional query parameters_ are `width` and `height`. Both are defaulted to 100 if not specified, as per spec. This request returns a resized image.

*Note:* Although the specification talks about JSON, I believe it is not required or desired here, for reasons outlined above.

## Limitations and further work

The most obvious drawback of this design is that scaling factor is a static parameter. In a real production scenario, one would want a more elastic approach to cope with fluctuations in demand in a cost-effective manner. One way to achieve this is to deploy the solution on Kubernetes and leverage horizontal pod autoscaling: Each pod (i.e. image resize worker instance) would emit "busyness" metrics that the pod autoscaler would use to spin up and tear down pods based on. This would also allow us to seamlessly scale the solution across multiple nodes.

Additionally, the actual image resizing functionality is very limited in this solution. An ideal thumbnail algorithm could pick a suitable rectangular area of the source image and scale it proportionally. A client may want to be able to generate thumbnails in different sizes, and perhaps even choose interpolation algorithm.


## How to run

    docker-compose up

    curl -X POST --data-binary "@data/sample.png" --header "Content-Type: image/png" "localhost:5000/resize" > out.png

## How to test

While there's also a small unit test, it only serves as a placeholder. There is very little business logic in this solution. More important is the integration test suite, which is run the following way:

    docker-compose up

    cd test
    virtualenv myinttestenv
    . myinttestenv/bin/activate
    pip install -r requirements.txt
    python -m unittest webtests.py -v

---

Submitted by Jacob Ehnmark.