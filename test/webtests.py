import unittest
import requests
import json

RESIZE_BASE_URL="http://localhost:5000/resize"
DEBUG_BASE_URL="http://localhost:5000/debug"

def getImageBytes():
    with open("sample.png", "rb") as fp:
        return fp.read()

# This is actually an integration tests, but
# we're using the unittest framework.
class ResizeTests(unittest.TestCase):

    def test_shouldFailWith400IfNoData(self):
        r = requests.post(RESIZE_BASE_URL)
        self.assertEqual(400, r.status_code)

    def test_shouldFailWith400IfNonIntParams(self):
        bytes = getImageBytes()
        url = "%s?width=100&height=foo" % RESIZE_BASE_URL
        r = requests.post(url, data=bytes)
        self.assertEqual(400, r.status_code)
        self.assertTrue("invalid" in r.text)

    def test_shouldFailWith400IfNonImagePayload(self):
        bytes = b'hello'
        url = "%s?width=100&height=100" % RESIZE_BASE_URL
        r = requests.post(url, data=bytes)
        self.assertEqual(400, r.status_code)

    def test_shouldResizeIfInputsValid(self):
        bytes = getImageBytes()
        url = "%s?width=100&height=100" % RESIZE_BASE_URL
        r = requests.post(url, data=bytes)
        self.assertEqual(200, r.status_code)
        self.assertEqual("image/png", r.headers["Content-Type"])
        self.assertTrue(len(r.content) > 0)


class ScalingTests(unittest.TestCase):

    def test_ShouldLoadBalanceRequests(self):
        pids = set()
        for i in range(20): 
            res = requests.get(DEBUG_BASE_URL).json()
            pids.add(res["pid"])
        self.assertGreater(len(pids), 1)


if __name__ == '__main__':
    unittest.main()